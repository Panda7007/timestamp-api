import os
import time
import cv2
import numpy as np
import datetime as dt
import imageio
import requests

# Define global variables
font = cv2.FONT_HERSHEY_COMPLEX
classes = ["Class1", "Class2"]  # Update with your actual class names
starting_time = time.time()
tanggal = dt.datetime.now().date()

# Define the codec and create VideoWriter object
fpsvideo = 24
output_folder = "video"
if not os.path.exists(output_folder):
    os.makedirs(output_folder)
output_video_path = os.path.join(output_folder, f'CCTV-{tanggal}.mp4')

# Create a writer object using ImageIO
out = imageio.get_writer(output_video_path, fps=fpsvideo)

# Define the output file path in the specific folder
timefolder = "timestamp"
if not os.path.exists(timefolder):
    os.makedirs(timefolder)
output_file = f"CCTV-orang-{str(tanggal)}-timestamp.txt"
output_file = os.path.join(timefolder, output_file)

# Create the evidence photo folder if it does not exist
bukti_foto_folder = "bukti_foto"
if not os.path.exists(bukti_foto_folder):
    os.makedirs(bukti_foto_folder)

# Setting for FPS display
starting_time = time.time()
frame_id = 0

# Setting the time
stamp1 = 0
stampout = 0

# Text caption to be sent along with the photo
caption_text = "Terdeteksi gerakan MENCURIGAKAN di kamera"

# Ganti 'URL_TARGET' dengan URL yang sesuai
telegram_url = 'https://api.telegram.org/bot6781633761:AAGw1NGbm1lsBFfNXQ8yxfw5IGG_OIFZcpA/sendPhoto'
data = {'chat_id': '-4062304369', 'caption': caption_text}

# Define the ObjectDetection class
class ObjectDetection:
    def __init__(self, cap, out_writer):
        PROJECT_PATH = os.path.abspath(os.getcwd())
        MODELS_PATH = os.path.join(PROJECT_PATH, "models")

        self.MODEL = cv2.dnn.readNetFromONNX(
            os.path.join(MODELS_PATH, "humanversi5.onnx")
        )

        # Initialize the video capture object
        self.cap = cap
        self.frame_count = 0  # Initialize frame_count attribute
        self.last_detik = 0
        self.out_writer = out_writer  # Store the ImageIO writer

    def detectObj(self, snap):
        global frame_id, stamp1, stampout, out_writer, last_detik  # Declare global variables
        img = snap.copy()
        frm = img.copy()
        frame_id += 1

        frame_rate = 2  # kemampuan laptop untuk deteksi

        if img is None:
            return snap

        img = cv2.resize(img, (1080, 720))
        blob = cv2.dnn.blobFromImage(img, scalefactor=1/255, size=(640, 640), mean=[0, 0, 0], swapRB=True, crop=False)
        self.MODEL.setInput(blob)
        detections = self.MODEL.forward()[0]

        classes_ids = []
        confidences = []
        boxes = []
        rows = detections.shape[0]

        img_width, img_height = img.shape[1], img.shape[0]
        x_scale = img_width / 640
        y_scale = img_height / 640

        for i in range(rows):
            row = detections[i]
            confidence = row[4]
            if confidence >= 0.25:
                classes_score = row[5:]
                ind = np.argmax(classes_score)
                if classes_score[ind] >= 0.25:
                    classes_ids.append(ind)
                    confidences.append(confidence)
                    cx, cy, w, h = row[:4]
                    x1 = int((cx - w/2) * x_scale)
                    y1 = int((cy - h/2) * y_scale)
                    width = int(w * x_scale)
                    height = int(h * y_scale)
                    box = np.array([x1, y1, width, height])
                    boxes.append(box)
        indices = cv2.dnn.NMSBoxes(boxes, confidences, 0.25, 0.25)

        waktu = time.asctime()
        jam = dt.datetime.now().hour
        tanggal = dt.datetime.now().date()

        elapsed_time = time.time() - starting_time
        fps = frame_id / elapsed_time

        cv2.putText(img, "Jumlah Orang : " + str(len(indices)), (10, 140), font, 1, (200, 0, 0), 1)
        cv2.putText(img, "Fps : " + str(fps), (10, 90), font, 1, (0, 255, 0), 1)
        #cv2.putText(img, "Waktu : " + str(waktu), (10, 50), font, 0.3, (200, 0, 0), 1)
        #cv2.putText(img, "Jam : " + str(jam), (10, 70), font, 0.3, (200, 0, 0), 1)

        if len(indices) > stamp1:
        
            # Simpan nomor frame yang terdeteksi ke dalam file teks
            with open(output_file, 'a') as file:
                file.write(str(int(frame_id)) + '\n')
            print("Posisi frame yang tercatat dalam database:", int(frame_id))
            detected_frames = []            


            # Kode untuk mengirimkan alarm dengan email
            if int(jam) < 7:
                # Menyimpan gambar ke dalam folder bukti_foto
                cv2.imwrite(os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png'), img)
                namafile = os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png')
                # Open the file again before sending the request
                with open(namafile, 'rb') as file:
                    files = {'photo': file}
                    response = requests.post(telegram_url, files=files, data=data)
                print("Email terkirim yaak")

            if jam > 19:
                # Menyimpan gambar ke dalam folder bukti_foto
                cv2.imwrite(os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png'), img)
                namafile = os.path.join(bukti_foto_folder, f'{tanggal}-Jam-{jam}-gambar.png')
                # Open the file again before sending the request
                with open(namafile, 'rb') as file:
                    files = {'photo': file}
                    response = requests.post(telegram_url, files=files, data=data)
                print("Email terkirim yaak")

            stamp1 = len(indices)

        if stamp1 > len(indices):
            stamp1 = len(indices)


        if len(indices) < stampout:
            # Check if the current detik is different from the previous one
                # Simpan nomor frame yang terdeteksi ke dalam file teks
                
            with open(output_file, 'a') as file:
                file.write(str(int(frame_id)) + '\n')
            print("Posisi frame yang tercatat dalam database stampout:", int(frame_id))
            
            detected_frames = []
            stampout = len(indices)

        if stampout < len(indices):
            stampout = len(indices)


        if len(indices) > 0:
            for i in indices:
                x1, y1, w, h = boxes[i]
                label = classes[classes_ids[i]]
                conf = confidences[i]
                text = label + "{:.2f}".format(conf)
                cv2.rectangle(img, (int(x1), int(y1)), (int(x1 + w), int(y1 + h)), (255, 0, 0), 2)
                cv2.putText(img, text, (int(x1), int(y1) - 2), cv2.FONT_HERSHEY_COMPLEX, 0.3, (0, 0, 255), 2)
                cv2.rectangle(frm, (int(x1), int(y1)), (int(x1 + w), int(y1 + h)), (255, 0, 0), 2)
                cv2.putText(frm, text, (int(x1), int(y1) - 2), cv2.FONT_HERSHEY_COMPLEX, 0.3, (0, 0, 255), 2)

        print("Posisi frame",int(frame_id))
        out.append_data(img)

        return img


# Define the VideoStreaming class
class VideoStreaming:
    def __init__(self):
        self.VIDEO = cv2.VideoCapture(r"C:\\Users\\Panda\\Documents\\Semester 7\\timestamp-sukses\\Video bahan\\Full 1 Jam H08.mp4")
        self.MODEL = ObjectDetection(self.VIDEO, out)
        self.last_detection_time = dt.datetime.now()
        self.processed_frames = 0
        self.frame_count = 0
        self._preview = True
        self._flipH = False
        self._detect = False
        self._exposure = self.VIDEO.get(cv2.CAP_PROP_EXPOSURE)
        self._contrast = self.VIDEO.get(cv2.CAP_PROP_CONTRAST)
        self._fps = self.VIDEO.get(cv2.CAP_PROP_FPS)

    def show(self):
        while self.VIDEO.isOpened():
            ret, snap = self.VIDEO.read()
            if self._flipH:
                snap = cv2.flip(snap, 1)

            if ret:
                if self._preview:
                    if self._detect:
                        snap = self.MODEL.detectObj(snap)
                        self.processed_frames += 1
                        current_time = dt.datetime.now()
                        time_difference = current_time - self.last_detection_time
                        if time_difference.total_seconds() >= 1:
                            fps = self.processed_frames / time_difference.total_seconds()
                            self.processed_frames = 0
                            self.last_detection_time = current_time
                        #out.close()
                else:
                    snap = np.zeros((
                        int(self.VIDEO.get(cv2.CAP_PROP_FRAME_HEIGHT)),
                        int(self.VIDEO.get(cv2.CAP_PROP_FRAME_WIDTH))
                    ), np.uint8)
                    label = "Video Sudah di save"
                    H, W = snap.shape
                    font = cv2.FONT_HERSHEY_PLAIN
                    color = (255, 255, 255)
                    cv2.putText(snap, label, (W // 2 - 100, H // 2), font, 6, color, 2)
                    out.close()

                actual_fps = self._fps
                font = cv2.FONT_HERSHEY_SIMPLEX
                text = f"FPS opo: {actual_fps:.2f}"
                #cv2.putText(snap, text, (10, 30), font, 1, (0, 255, 0), 2)

                frame = cv2.imencode(".jpg", snap)[1].tobytes()
                yield (b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
                time.sleep(0.01)

            else:
                break

        

    @property
    def preview(self):
        return self._preview


    @preview.setter
    def preview(self, value):
        self._preview = bool(value)

    @property
    def flipH(self):
        return self._flipH

    @flipH.setter
    def flipH(self, value):
        self._flipH = bool(value)

    @property
    def detect(self):
        return self._detect

    @detect.setter
    def detect(self, value):
        self._detect = bool(value)

    @property
    def exposure(self):
        return self._exposure

    @exposure.setter
    def exposure(self, value):
        self._exposure = value
        self.VIDEO.set(cv2.CAP_PROP_EXPOSURE, self._exposure)

    @property
    def contrast(self):
        return self._contrast

    @contrast.setter
    def contrast(self, value):
        self._contrast = value
        self.VIDEO.set(cv2.CAP_PROP_CONTRAST, self._contrast)

if __name__ == "__main__":
    video_stream = VideoStreaming()
    # Register a function to close resources when the program exits
    atexit.register(video_stream.VIDEO.release)
    atexit.register(out.close)  # Close the ImageIO writer
    video_stream.show()
