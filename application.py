from flask import Flask, render_template, Response
from flask_bootstrap import Bootstrap
from flask import current_app

from object_detection import VideoStreaming
from camera_settings import reset_settings

application = Flask(__name__)
Bootstrap(application)

# Initialize VideoStreaming instance
application.config["VIDEO_INSTANCE"] = VideoStreaming()


@application.route("/putar_video")
def putar():
    TITLE = "Putar Video"
    return render_template("play.html", TITLE=TITLE)



@application.route("/")
def home():
    TITLE = "CCTV - object detection"
    return render_template("index.html", TITLE=TITLE)


@application.route("/video_feed")
def video_feed():
    try:
        video_instance = current_app.config.get("VIDEO_INSTANCE")
        return Response(
            video_instance.show(),
            mimetype="multipart/x-mixed-replace; boundary=frame"
        )
    except Exception as e:
        print("Error in video_feed:", str(e))
        return "Error during video streaming."


# Button requests
@application.route("/request_preview_switch")
def request_preview_switch():
    video_instance = current_app.config.get("VIDEO_INSTANCE")
    video_instance.preview = not video_instance.preview
    print("*" * 10, video_instance.preview)
    return "nothing"


@application.route("/request_flipH_switch")
def request_flipH_switch():
    video_instance = current_app.config.get("VIDEO_INSTANCE")
    video_instance.flipH = not video_instance.flipH
    print("*" * 10, video_instance.flipH)
    return "nothing"


@application.route("/request_model_switch")
def request_model_switch():
    video_instance = current_app.config.get("VIDEO_INSTANCE")
    video_instance.detect = not video_instance.detect
    print("*" * 10, video_instance.detect)
    return "nothing"


@application.route("/request_exposure_down")
def request_exposure_down():
    video_instance = current_app.config.get("VIDEO_INSTANCE")
    video_instance.exposure -= 1
    print("*" * 10, video_instance.exposure)
    return "nothing"


@application.route("/request_exposure_up")
def request_exposure_up():
    video_instance = current_app.config.get("VIDEO_INSTANCE")
    video_instance.exposure += 1
    print("*" * 10, video_instance.exposure)
    return "nothing"


@application.route("/request_contrast_down")
def request_contrast_down():
    video_instance = current_app.config.get("VIDEO_INSTANCE")
    video_instance.contrast -= 4
    print("*" * 10, video_instance.contrast)
    return "nothing"


@application.route("/request_contrast_up")
def request_contrast_up():
    video_instance = current_app.config.get("VIDEO_INSTANCE")
    video_instance.contrast += 4
    print("*" * 10, video_instance.contrast)
    return "nothing"


@application.route("/reset_camera")
def reset_camera():
    STATUS = reset_settings()
    print("*" * 10, STATUS)
    return "nothing"


if __name__ == "__main__":
    application.run(debug=True)
